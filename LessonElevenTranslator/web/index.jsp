<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1 style="text-align: center;">Translator</h1>
<table align="center">
    <tr style="font-weight: bold; text-align: center;">
        <td>Initial text</td>
        <td></td>
        <td>Translated text</td>
    </tr>
    <tr>
        <form action="/eleven/translate" method="get">
            <td>
                <textarea rows="10" cols="45" name="text"></textarea><br>
            </td>
            <td>
                <input type="submit" value="Translate ->">
            </td>
        </form>
        </td>
        <td>
            <textarea rows="10" cols="45"><c:out value="${translatedText}"/></textarea>
        </td>
    </tr>
</table>
</body>
</html>