package com.main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@org.springframework.stereotype.Controller()
public class Controller {

    @Autowired
    Translator translator;

    @RequestMapping("/translate")
    public ModelAndView hello (HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("translatedText", translator.translate(request.getParameter("text")));
        return modelAndView;
    }
}
