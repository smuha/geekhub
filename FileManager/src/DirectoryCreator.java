import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

/**
 * Created by Admin on 30.01.14.
 */
@WebServlet("/createDirectory")
public class DirectoryCreator extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        HttpSession session = request.getSession();

        File file = new File(session.getAttribute("currentPath") + File.separator + request.getParameter("name"));
        file.mkdir();

        session.setAttribute("names", ContentScanner.getFoldersList((String) session.getAttribute("currentPath")));
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/fileManager.jsp");
        dispatcher.forward(request, response);
    }
}
