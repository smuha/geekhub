import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by Admin on 29.01.14.
 */
@WebServlet("/RootPath")
public class CreateRootPath extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html; charset=utf-8");
        HttpSession session = request.getSession();
        ServletContext context = request.getSession().getServletContext();
        session.setAttribute("currentPath", Paths.get(context.getRealPath(context.getInitParameter("root"))).toString() + File.separator);
        session.setAttribute("rootPath", Paths.get(context.getRealPath(context.getInitParameter("root"))).toString() + File.separator);
        String[] items = ContentScanner.getFoldersList((String) session.getAttribute("currentPath"));
        session.setAttribute("names", items);

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/fileManager.jsp");
        dispatcher.forward(request, response);
    }
}