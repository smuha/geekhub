<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
</head>
<body>
<div style="text-align: center;">
    <h2>Hi, here is an index page. If you want to go to the file manager,</h2>
    <h2>and use its opportunities, click the following button</h2>
    <form method="get" action="/nine/RootPath" accept-charset="utf-8">
        <input type="submit" value="Go to File Manager" size="50">
    </form>
</div>
</body>
</html>