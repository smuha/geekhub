<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1 style="text-align: center;">File Manager</h1>
<table align="center" border="2" style="width: 550px; height: 500px">
    <tr style="height: 20px;">
        <td>
            Current path: "<%=session.getAttribute("currentPath")%>" <br>
        </td>
    </tr>
    <tr style="height: 30px;">
        <td style="text-align: center;">
            <a href="/nine/back"><img src="/nine/images/back.png" width="20">BACK</a>
            &nbsp;&nbsp;
            <a href="/nine/index.jsp">LEAVE MANAGER</a>
        </td>
    </tr>
    <tr valign="top">
        <td>
            <c:forEach var="name" items="${names}">
                &nbsp;<a href="/nine/remove?path=${name}">remove - </a>
                <a style="margin-left: 10px" href="/nine/fm?path=${name}">${name}</a><br>
            </c:forEach>
        </td>
    </tr>
    <tr style="height: 20px">
        <td>
            <form action="/nine/createFile">
                File name: <input type="text" name="name" size="10">
                File extension: <input type="text" name="extension" size="10">
                <input value="Create new file" type="submit">
            </form>
        </td>
    </tr>
    <tr style="height: 20px">
        <td>
            <form action="/nine/createDirectory">
                Directory name: <input type="text" name="name" size="10">
                <input value="Create new directory" type="submit">
            </form>
        </td>
    </tr>
</table>
<div style="text-align: center;">
    <br>(P.S: If directory isn't empty, it can't be removed)
</div>
</body>
</html>