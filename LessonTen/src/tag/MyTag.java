package tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;

public class MyTag extends SimpleTagSupport {
    private HashMap<String, HashSet<String>> myMap;

    public void setMyMap(HashMap<String, HashSet<String>> arg) {
        this.myMap = arg;
    }

    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        if (myMap != null) {
            boolean outName;
            for (String kay : myMap.keySet()) {
                outName = true;
                out.print("<tr><td>" + kay + "</td>");
                for (String val : myMap.get(kay)) {
                    if (outName) {
                        out.print("<td>" + val + "</td><td><a href='/ten/Delete?value=" + val + "&name=" + kay + "'>delete</a></td></tr>");
                        outName = false;
                    } else {
                        out.print("<tr><td></td><td>" + val + "</td><td><a href='/ten/Delete?value=" + val + "&name=" + kay + "'>delete</a></td></tr>");
                    }
                }
            }
        }
    }
}