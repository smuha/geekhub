import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Admin on 15.02.14.
 */
@WebServlet("/Delete")
public class Delete extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String name = request.getParameter("name");
        String value = request.getParameter("value");

        ((HashMap<String, HashSet<String>>) session.getAttribute("myMap")).get(name).remove(value);
        if (((HashMap<String, HashSet<String>>) session.getAttribute("myMap")).get(name).isEmpty()) {
            ((HashMap<String, HashSet<String>>) session.getAttribute("myMap")).remove(name);
        }

        getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
