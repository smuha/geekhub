import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Created by Admin on 10.02.14.
 */
@WebServlet("/Add")
public class AddData extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String name = request.getParameter("name");
        String value = request.getParameter("value");

        if (session.getAttribute("myMap") == null) {
            HashMap<String, HashSet<String>> map = new HashMap<>();
            session.setAttribute("myMap", map);
        }

        HashMap<String, HashSet<String>> myMap = (HashMap<String, HashSet<String>>) session.getAttribute("myMap");
        HashSet<String> list;
        if (!myMap.containsKey(name)) {
            list = new HashSet<>();
            list.add(value);
            myMap.put(name, list);
        } else {
            list = myMap.get(name);
            list.add(value);
        }

        getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
