<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ex" uri="/WEB-INF/custom.tld"%>
<html>
<head>
    <title></title>
</head>
<body>
<h1 style="text-align: center;">
    Lesson Ten
</h1>
<table align="center" valign="top" border="2" style="width: 500px;">
    <tr style="text-align: center; font-weight:bold;">
        <td>
            Name
        </td>
        <td>
            Value
        </td>
        <td>
            Action
        </td>
    </tr>

    <ex:myOut myMap="${myMap}"/>

    <tr>
        <form action="/ten/Add">
            <td>
                <input name="name" type="text">
            </td>
            <td>
                <input name="value" type="text">
            </td>
            <td>
                <input type="submit" value="add">
            </td>
        </form>
    </tr>
</table>
</body>
</html>