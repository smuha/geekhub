<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="<c:url value="/res/js/jquery-2.1.0.min.js" />"></script>
    <script src="<c:url value="/res/js/jquery.validate.min.js" />"></script>
    <script src="<c:url value="/res/js/myValidation.js" />"></script>
</head>
<body>
<h1>Groups list</h1>

<p>
    <form id="addGroupForm" action="${pageContext.request.contextPath}/groups" method="post">
        <input name="name" placeholder="Name"><br/>
        <input type="submit" value="Submit">
    </form>
</p>

<h3>Ajax</h3>
<div id="addGroupDiv">
    <input id="groupName" name="name" placeholder="Name"><br/>
    <input id="addGroup" type="button" value="Add group"/>
</div>

<table border="1">
    <tr>
        <td>ID</td>
        <td>NAME</td>
        <td>USERS</td>
        <td>EDIT</td>
        <td>DELETE</td>
    </tr>

    <c:forEach var="group" items="${groups}">
        <tr>
            <td>${group.id}</td>
            <td>${group.name}</td>
            <td>
                <c:forEach items="${group.users}" var="user">
                    ${user.firstName} ${user.lastName}<br>
                </c:forEach>
            </td>
            <td><a href="editGroup?id=${group.id}">Edit</a></td>
            <td><a href="deleteGroup?id=${group.id}">Delete</a></td>
        </tr>
    </c:forEach>

</table>
<a href="${pageContext.request.contextPath}/users">User list</a>

<script language="JavaScript">
    $("#addGroup").click(function () {
        $.post("http://localhost:8080/HibernateExample/groups",
                { name: $("#groupName").val()},
                function (data) {
                    document.body.innerHTML = data;
                }
        );
    });
</script>
</body>
</html>