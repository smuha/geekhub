<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="<c:url value="/res/js/jquery-2.1.0.min.js" />"></script>
    <script src="<c:url value="/res/js/jquery.validate.min.js" />"></script>
    <script src="<c:url value="/res/js/myValidation.js" />"></script>
</head>
<body>
<h1>Edit group</h1>
<table>
    <form id="changeGroup" action="${pageContext.request.contextPath}/updateGroup" method="post">
        <tr>
            <td>Group name</td>
            <td><input name="name" value="${group.name}"><br></td>
        </tr>
        <tr>
            <td>Users list</td>
            <td>
                <c:forEach items="${users}" var="user">
                    <c:set var="flag" scope="session" value="${false}"/>
                    <c:forEach items="${group.users}" var="selectedUser">
                        <c:if test="${selectedUser.id == user.id}">
                            <c:set var="flag" scope="session" value="${true}"/>
                        </c:if>
                    </c:forEach>
                    <c:choose>
                        <c:when test="${flag == true}">
                            <input type="checkbox" name="usersId" value="${user.id}" checked>${user.firstName}<br>
                        </c:when>
                        <c:otherwise>
                            <input type="checkbox" name="usersId" value="${user.id}">${user.firstName}<br>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <input type="hidden" name="id" value="${group.id}">
            <td colspan="2"><input type="submit" value="Submit"></td>
        </tr>
    </form>
</table>
<a href="${pageContext.request.contextPath}/groups">Group list</a>
</body>
</html>