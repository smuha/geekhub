<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="<c:url value="/res/js/jquery-2.1.0.min.js" />"></script>
    <script src="<c:url value="/res/js/jquery.validate.min.js" />"></script>
    <script src="<c:url value="/res/js/myValidation.js" />"></script>
</head>
<body>
<h1>Edit user</h1>
<form id="changeUser" action="${pageContext.request.contextPath}/updateUser" method="post">
    <input name="firstName" value="${user.firstName}"><br />
    <input name="lastName" value="${user.lastName}"><br />
    <input name="email" value="${user.email}"><br />
    <select name='groupId'>
        <option value="${user.group.id}" selected>${user.group.name}</option>
        <c:forEach items="${groups}" var="group">
            <option value="${group.id}">${group.name}</option>
        </c:forEach>
    </select><br />
    <input type="hidden" name="id" value="${user.id}">
    <input type="submit" value="Submit">
</form>

<h3>Ajax</h3>
<div id="divChangeUser">
    <input name="firstName" value="${user.firstName}"><br />
    <input name="lastName" value="${user.lastName}"><br />
    <input name="email" value="${user.email}"><br />
    <select name='groupId'>
        <option value="${user.group.id}" selected>${user.group.name}</option>
        <c:forEach items="${groups}" var="group">
            <option value="${group.id}">${group.name}</option>
        </c:forEach>
    </select><br />
    <input type="hidden" name="id" value="${user.id}">
    <input id="changeUserButton" type="button" value="Change user">
</div>

<script language="JavaScript">
    $( "#changeUserButton" ).click(function() {
        $.post("http://localhost:8080/HibernateExample/updateUser",
                {
                    firstName: $( "#divChangeUser").find("input[name$='firstName']").val(),
                    lastName: $( "#divChangeUser").find("input[name$='lastName']").val(),
                    email: $( "#divChangeUser").find("input[name$='email']").val(),
                    groupId: $( "#divChangeUser").find("select[name$='groupId']").val(),
                    id: $( "#divChangeUser").find("input[name$='id']").val()
                },
                function(data) {
                    document.body.innerHTML = data;
                }
        );
    });
</script>

<a href="${pageContext.request.contextPath}/users">User list</a>
</body>
</html>