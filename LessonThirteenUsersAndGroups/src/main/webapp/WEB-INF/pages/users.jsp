<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script src="<c:url value="/res/js/jquery-2.1.0.min.js" />"></script>
    <%--<script src="<c:url value="/res/js/jquery-2.1.0.js" />"></script>--%>
    <script src="<c:url value="/res/js/jquery.validate.min.js" />"></script>
    <script src="<c:url value="/res/js/myValidation.js" />"></script>
</head>
<body>
	<h1>Users list</h1>

    <form id="addUserForm" action="${pageContext.request.contextPath}/users" method="post">
        <input type="text" name="firstName" placeholder="First Name"><br />
        <input type="text" name="lastName" placeholder="Last Name"><br />
        <input name="email" type="email" placeholder="Email"><br />
        <select name='groupId'>
            <c:forEach items="${groups}" var="group">
                <option value="${group.id}">${group.name}</option>
            </c:forEach>
        </select><br />
        <input type="submit" value="Submit"><br />
    </form>

    <h3>Ajax</h3>
	<div id="addUserDiv">
		<input id="firstName" type="text" name="firstName" placeholder="First Name"><br />
		<input id="lastName" type="text" name="lastName" placeholder="Last Name"><br />
		<input id="email" name="email" type="email" placeholder="Email"><br />
        <select id="groupId" name='groupId'>
            <c:forEach items="${groups}" var="group">
                <option value="${group.id}">${group.name}</option>
            </c:forEach>
        </select><br />
        <input id="addUserButton" type="button" value="Add user Ajax" />
	</div>

	<table border="1">
		<tr>
			<td>ID</td>
			<td>FIRST NAME</td>
			<td>LAST NAME</td>
			<td>EMAIL</td>
			<td>GROUP</td>
			<td>UPDATE</td>
			<td>DELETE</td>
		</tr>

		<c:forEach var="user" items="${users}">
		<tr>
			<td>${user.id}</td>
			<td>${user.firstName}</td>
			<td>${user.lastName}</td>
			<td>${user.email}</td>
			<td>${user.group.name}</td>
            <td><a href="editUser?id=${user.id}">Edit</a></td>
            <td><a href="deleteUser?id=${user.id}">Delete</a></td>
		</tr>
		</c:forEach>
	</table>

    <a href="${pageContext.request.contextPath}/groups">Groups list</a>

    <script language="JavaScript">
        $( "#addUserButton" ).click(function() {
            $.post("http://localhost:8080/HibernateExample/users",
                    {
                        firstName: $("#firstName").val(),
                        lastName: $("#lastName").val(),
                        email: $("#email").val(),
                        groupId: $("#groupId").val()
                    },
                    function(data) {
                        document.body.innerHTML = data;
                    }
            );
        });
    </script>
</body>
</html>