$(document).ready(function () {
    $("#addUserForm").validate({

        rules: {
            firstName: {
                required: true,
                minlength: 2,
                maxlength: 16
            },
            lastName: {
                required: true,
                minlength: 2,
                maxlength: 16
            },
            email: {
                required: true,
                email: true
            }
        },

        messages: {

            firstName: {
                required: "This field is required",
                minlength: "Min length is 2 symbols",
                maxlength: "Min length is 16 symbols"
            },

            lastName: {
                required: "This field is required",
                minlength: "Min length is 2 symbols",
                maxlength: "Min length is 16 symbols"
            },

            email: {
                required: "This field is required",
                email: "This isn't email format"
            }
        }
    });

    $("#changeUser").validate({

        rules: {
            firstName: {
                required: true,
                minlength: 2,
                maxlength: 16
            },
            lastName: {
                required: true,
                minlength: 2,
                maxlength: 16
            },
            email: {
                required: true,
                email: true
            }
        },

        messages: {

            firstName: {
                required: "This field is required",
                minlength: "Min length is 2 symbols",
                maxlength: "Min length is 16 symbols"
            },

            lastName: {
                required: "This field is required",
                minlength: "Min length is 2 symbols",
                maxlength: "Min length is 16 symbols"
            },

            email: {
                required: "This field is required",
                email: "This isn't email format"
            }
        }
    });

    $("#addGroupForm").validate({

        rules: {
            name: {
                required: true,
                minlength: 2,
                maxlength: 25,
                equalTo: ""
            }
        },

        messages: {
            name: {
                required: "This field is required",
                minlength: "Min length is 2 symbols",
                maxlength: "Min length is 25 symbols",
                equalTo: "Field can't be empty"
            }
        }
    });

    $("#changeGroup").validate({

        rules: {
            name: {
                required: true,
                minlength: 2,
                maxlength: 25
            }
        },

        messages: {
            name: {
                required: "This field is required",
                minlength: "Min length is 2 symbols",
                maxlength: "Min length is 25 symbols"
            }
        }
    });
});