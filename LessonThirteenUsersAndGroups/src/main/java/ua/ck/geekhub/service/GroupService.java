package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.entity.Group;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Admin on 13.03.14.
 */
@Repository
@Transactional
public class GroupService {
    @Autowired
    SessionFactory sessionFactory;

    public void saveOrUpdateGroup(Group group) {
        sessionFactory.getCurrentSession().saveOrUpdate(group);
    }

    public Group getGroup(Integer id) {
        return (Group) sessionFactory.getCurrentSession().get(Group.class, id);
    }

    public List<Group> getGroups() {
        return sessionFactory.getCurrentSession()
                .createQuery("FROM Group")  //.createCriteria(Group.class)
                .list();
    }

    public void deleteGroup(Group group) {
        sessionFactory.getCurrentSession().delete(group);
    }

    public void createGroup(String name) {
        Group group = new Group();
        group.setName(name);
        saveOrUpdateGroup(group);
    }
}
