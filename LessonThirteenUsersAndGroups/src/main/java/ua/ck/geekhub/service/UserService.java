package ua.ck.geekhub.service;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.entity.User;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class UserService {

	@Autowired
	SessionFactory sessionFactory;

    @Autowired
    GroupService groupService;

	public void saveOrUpdateUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);
//        Group group = user.getGroup();
//        group.getUsers().add(user);
//        sessionFactory.getCurrentSession().saveOrUpdate(group);
	}

	public User getUser(Integer id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	public List<User> getUsers() {
		return sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .list();
	}

    public void deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }

	public void createUser(String firstName, String lastName, String email, String groupId) {
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
        user.setGroup(groupService.getGroup(Integer.parseInt(groupId)));
		saveOrUpdateUser(user);
	}

}
