package ua.ck.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.UserRoleAuthorizationInterceptor;
import ua.ck.geekhub.entity.Group;
import ua.ck.geekhub.entity.User;
import ua.ck.geekhub.service.GroupService;
import ua.ck.geekhub.service.UserService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class HelloController {

	@Autowired
    UserService userService;

    @Autowired
    GroupService groupService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		return "redirect:groups";
	}

	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String user(ModelMap model) {
        System.out.println("You've passed to the users. Congratulate you!");
        model.addAttribute("users", userService.getUsers());
        model.addAttribute("groups", groupService.getGroups());
        return "users";
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public String createUser(
			@RequestParam String firstName,
	        @RequestParam String lastName,
	        @RequestParam String email,
	        @RequestParam String groupId
	) {
		userService.createUser(firstName, lastName, email, groupId);
		return "redirect:users";
	}

    @RequestMapping(value = "/groups", method = RequestMethod.GET)
    public String groups(ModelMap model) {
        model.addAttribute("groups", groupService.getGroups());
        return "groups";
    }

    @RequestMapping(value = "/groups", method = RequestMethod.POST)
    public String createGroup(
            @RequestParam String name
    ) {
        groupService.createGroup(name);
        return "redirect:groups";
    }

    @RequestMapping(value = "/deleteGroup", method = RequestMethod.GET)
    public String deleteGroup(@RequestParam String id) {
        groupService.deleteGroup(groupService.getGroup(Integer.parseInt(id)));
        return "redirect:groups";
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public String deleteUser(@RequestParam String id) {
        userService.deleteUser(userService.getUser(Integer.parseInt(id)));
        return "redirect:users";
    }

    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public ModelAndView editUser(@RequestParam String id) {
        ModelAndView modelAndView = new ModelAndView("editUser");
        modelAndView.addObject("user", userService.getUser(Integer.parseInt(id)));
        modelAndView.addObject("groups", groupService.getGroups());
        return modelAndView;
    }

    @RequestMapping(value = "/editGroup", method = RequestMethod.GET)
    public ModelAndView editGroup(@RequestParam String id) {
        ModelAndView modelAndView = new ModelAndView("editGroup");
//        Group group = groupService.getGroup(Integer.parseInt(id));
//        List<User> users = userService.getUsers();
//        List<User> deleteUsersList = new ArrayList<>();
//        for (User user : users) {
//            for (User userInGroup : group.getUsers()) {
//                if (user.getId() == userInGroup.getId()) {
//                    deleteUsersList.add(user);
//                }
//            }
//        }
//        for (User user : deleteUsersList) {
//            users.remove(user);
//        }
        modelAndView.addObject("group", groupService.getGroup(Integer.parseInt(id)));
        modelAndView.addObject("users", userService.getUsers());
        return modelAndView;
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public String updateUser(
            @RequestParam String id,
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String email,
            @RequestParam String groupId
    ) {
        User user = new User();
        user.setId(Integer.parseInt(id));
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setGroup(groupService.getGroup(Integer.parseInt(groupId)));
        userService.saveOrUpdateUser(user);
        return "redirect:users";
    }

    @RequestMapping(value = "/updateGroup", method = RequestMethod.POST)
    public String updateGroup(
            @RequestParam String id,
            @RequestParam String name,
            @RequestParam String[] usersId
    ) {
        Group group = new Group();
        group.setId(Integer.parseInt(id));
        group.setName(name);
        groupService.saveOrUpdateGroup(group);
        User user;
        for (String userId : usersId) {
            user = userService.getUser(Integer.parseInt(userId));
            user.setGroup(group);
            userService.saveOrUpdateUser(user);
        }
        return "redirect:groups";
    }
}