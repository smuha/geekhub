package ua.ck.geekhub.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "GROUP_USER")
public class Group {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private int id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER, cascade={CascadeType.ALL}) //"group" is a field in User class
    private Set<User> users;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

}
