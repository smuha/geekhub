# --------------------------------------------------------
# Host:                         127.0.0.1
# Server version:               5.1.30-community
# Server OS:                    Win32
# HeidiSQL version:             6.0.0.3603
# Date/time:                    2014-03-10 01:12:45
# --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

# Dumping database structure for userdb
CREATE DATABASE IF NOT EXISTS `userdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `userdb`;


# Dumping structure for table userdb.actor
CREATE TABLE IF NOT EXISTS `actor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

# Dumping data for table userdb.actor: ~6 rows (approximately)
/*!40000 ALTER TABLE `actor` DISABLE KEYS */;
INSERT INTO `actor` (`id`, `firstName`, `lastName`, `birthday`) VALUES
	(12, 'Valentin', 'Mukha', '2014-03-09'),
	(16, 'Slava', 'Boyko', '2014-03-09'),
	(19, 'Adam', 'Sendler', '2014-03-09'),
	(26, 'Slava', 'Mukha', '2014-03-05'),
	(27, 'Anton', 'Shapoval', '1994-08-19');
/*!40000 ALTER TABLE `actor` ENABLE KEYS */;


# Dumping structure for table userdb.actors_and_movies
CREATE TABLE IF NOT EXISTS `actors_and_movies` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `actorId` int(10) DEFAULT NULL,
  `movieId` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `actorId_movieId` (`actorId`,`movieId`),
  KEY `FK_actors_and_movies_movie` (`movieId`),
  CONSTRAINT `FK_actors_and_movies_actor` FOREIGN KEY (`actorId`) REFERENCES `actor` (`id`),
  CONSTRAINT `FK_actors_and_movies_movie` FOREIGN KEY (`movieId`) REFERENCES `movie` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;

# Dumping data for table userdb.actors_and_movies: ~11 rows (approximately)
/*!40000 ALTER TABLE `actors_and_movies` DISABLE KEYS */;
INSERT INTO `actors_and_movies` (`id`, `actorId`, `movieId`) VALUES
	(14, 12, 37),
	(36, 12, 39),
	(15, 16, 37),
	(20, 19, 33),
	(21, 19, 37),
	(22, 19, 38),
	(37, 19, 39),
	(34, 26, 38),
	(35, 27, 33),
	(38, 27, 39);
/*!40000 ALTER TABLE `actors_and_movies` ENABLE KEYS */;


# Dumping structure for table userdb.movie
CREATE TABLE IF NOT EXISTS `movie` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `createDate` date DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

# Dumping data for table userdb.movie: ~4 rows (approximately)
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
INSERT INTO `movie` (`id`, `name`, `createDate`, `author`) VALUES
	(33, 'Good job', '2014-03-09', 'Altan Jon'),
	(37, 'Grean Mile', '2014-03-09', 'Jhon Correl'),
	(38, '1+1', '2014-03-09', 'Will Smitt'),
	(39, 'War', '2014-03-20', 'Slava Mukha');
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;


# Dumping structure for trigger userdb.deleteActor
SET SESSION SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `deleteActor` BEFORE DELETE ON `actor` FOR EACH ROW BEGIN
DELETE FROM actors_and_movies
    WHERE actors_and_movies.actorId = old.id;
END//
DELIMITER ;
SET SESSION SQL_MODE=@OLD_SQL_MODE;


# Dumping structure for trigger userdb.deleteMovie
SET SESSION SQL_MODE='STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `deleteMovie` BEFORE DELETE ON `movie` FOR EACH ROW BEGIN
DELETE FROM actors_and_movies
    WHERE actors_and_movies.movieId = old.id;
END//
DELIMITER ;
SET SESSION SQL_MODE=@OLD_SQL_MODE;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
