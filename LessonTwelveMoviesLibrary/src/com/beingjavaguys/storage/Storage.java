package com.beingjavaguys.storage;

import com.beingjavaguys.entity.Entity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface Storage {

    <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception;

    <T extends Entity> List<T> list(Class<T> clazz) throws Exception;

    <T extends Entity> boolean delete(String tableName, String id) throws Exception;

    <T extends Entity> void save(T entity) throws Exception;
}
