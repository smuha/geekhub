package com.beingjavaguys.storage;

import com.beingjavaguys.entity.Actor;
import com.beingjavaguys.entity.Entity;
import com.beingjavaguys.entity.Ignore;
import com.beingjavaguys.entity.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DatabaseStorage implements Storage {

    private Connection connection;

    @Autowired
    public void setDataSource (DataSource dataSource) throws SQLException {
        this.connection=dataSource.getConnection();
    }

    @PreDestroy
    public void destroy() throws SQLException {
        connection.close();
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    public Actor getActorWithMovies(Integer id) throws Exception {
        Actor actor = get(Actor.class, id);
        String sql = "select * from movie as m inner join actors_and_movies" +
                " as am on m.id=am.movieId where am.actorId=?";

        try(PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setInt(1, actor.getId());
                actor.setMovies(extractResult(Movie.class, preparedStatement.executeQuery()));
        }
        return actor;
    }

    public Movie getMovieWithActors(Integer id) throws Exception {
        Movie movie= get(Movie.class, id);
        String sql = "select * from actor as a inner join actors_and_movies" +
                " as am on a.id=am.actorId where am.movieId=?";

        try(PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, movie.getId());
            movie.setActors(extractResult(Actor.class, preparedStatement.executeQuery()));
        }
        return movie;
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql));
        }
    }

    @Override
    public <T extends Entity> boolean delete(String tableName, String id) throws Exception {
        String sql = "DELETE FROM " + tableName + " WHERE ID = " + id;
        try (Statement statement = connection.createStatement()) {
            return statement.executeUpdate(sql) == 0 ? false : true;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        if (entity.isNew()) {
            insertData(entity, data);
        } else {
            updateData(entity, data);
        }
    }

    public void saveMovieActors(Movie movie) throws Exception {
        String deleteSql = "delete from actors_and_movies where movieId" +
                "=" + movie.getId();
        String sql = "insert into actors_and_movies (actorId, movieId)" +
                " values (?, ?)";

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(deleteSql);
        }

        try(PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            for (Actor actor : movie.getActors()) {
                preparedStatement.setInt(1, actor.getId());
                preparedStatement.setInt(2, movie.getId());
                preparedStatement.execute();
            }
        }
    }

    public void saveActorMovies(Actor actor) throws Exception {
        String deleteSql = "delete from actors_and_movies where actorId" +
                "=" + actor.getId();
        String sql = "insert into actors_and_movies (actorId, movieId)" +
                " values (?, ?)";

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(deleteSql);
        }

        try(PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            for (Movie movie : actor.getMovies()) {
                preparedStatement.setInt(1, actor.getId());
                preparedStatement.setInt(2, movie.getId());
                preparedStatement.execute();
            }
        }
    }

    private <T extends Entity> void updateData(T entity, Map<String, Object> data) throws SQLException {
        String sql;
        sql = "UPDATE " + entity.getClass().getSimpleName() + " SET ";
        int counter = 0;
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            counter++;
            sql += entry.getKey() + "='" + entry.getValue() + "'";
            if (counter < data.size()) {
                sql += ", ";
            }
        }
        sql += " WHERE ID = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    private <T extends Entity> void insertData(T entity, Map<String, Object> data) throws Exception {
        String sql;
        StringBuilder fieldsValues = new StringBuilder();
        int i = 0;
        for (Object o : data.values()) {
            i++;
            if (o == null) {
                fieldsValues.append(o);
            } else {
                fieldsValues.append("'" + o + "'");
            }
            if (i < data.size()) {
                fieldsValues.append(", ");
            }
        }

        sql = "INSERT INTO " + entity.getClass().getSimpleName() + " (" + data.keySet().toString() +
                ") VALUES ( " + fieldsValues + " )";
        sql = sql.replace("[", "");
        sql = sql.replace("]", "");

        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet result = statement.getGeneratedKeys();
            result.first();
            entity.setId(result.getInt(1));
        }
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> map = new HashMap<>();

        for (Field field : entity.getClass().getDeclaredFields()) {
            if (!field.isAnnotationPresent(Ignore.class)) {
                Method getter = new PropertyDescriptor(field.getName(), entity.getClass()).getReadMethod();
                map.put(field.getName(), getter.invoke(entity));
            }
        }
        return map;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        List<T> list = new ArrayList<>();
        while (resultset.next()) {
            T o = clazz.newInstance();
            o.setId(resultset.getInt("id"));
            for (Field field : o.getClass().getDeclaredFields()) {
                if (field.isAnnotationPresent(Ignore.class)) continue;
                field.setAccessible(true);
                field.set(o, resultset.getObject(field.getName()));
            }
            list.add(o);
        }
        return list;
    }

    public List<Movie> movieList() throws Exception {
        List<Movie> movies = list(Movie.class);
        String sql = "select * from actor as a inner join actors_and_movies" +
                " as am on a.id=am.actorId where am.movieId=?";

        try(PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            for (Movie movie : movies) {
                preparedStatement.setInt(1, movie.getId());
                movie.setActors(extractResult(Actor.class, preparedStatement.executeQuery()));
            }
        }

        return movies;
    }

    public List<Actor> actorList() throws Exception {
        List<Actor> actors = list(Actor.class);
        String sql = "select * from movie as m inner join actors_and_movies" +
                " as am on m.id=am.movieId where am.actorId=?";

        try(PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            for (Actor actor : actors) {
                preparedStatement.setInt(1, actor.getId());
                actor.setMovies(extractResult(Movie.class, preparedStatement.executeQuery()));
            }
        }

        return actors;
    }
}
