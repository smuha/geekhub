package com.beingjavaguys.controller;

import com.beingjavaguys.entity.Actor;
import com.beingjavaguys.entity.Movie;
import com.beingjavaguys.storage.DatabaseStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class HomePageController {

    @Autowired
    DatabaseStorage databaseStorage;

    @RequestMapping("/addActor")
    public ModelAndView addActor() throws Exception {
        List<Movie> movies = databaseStorage.list(Movie.class);
        return new ModelAndView("addActor", "movies", movies);
    }

    @RequestMapping("/addMovie")
    public ModelAndView addMovie() throws Exception {
        List<Actor> actors = databaseStorage.list(Actor.class);
        return new ModelAndView("addMovie", "actors", actors);
    }

    @RequestMapping("/saveActor")
    public String saveActor(HttpServletRequest request) throws Exception {
        Actor actor = new Actor();
        actor.setFirstName(request.getParameter("firstName"));
        actor.setLastName(request.getParameter("lastName"));

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = format.parse(request.getParameter("birthday"));
        actor.setBirthday(new java.sql.Date(parsed.getTime()));

        for (String actorId : request.getParameterValues("selectedMovies")) {
            actor.addMovie(databaseStorage.get(Movie.class, Integer.parseInt(actorId)));
        }

        databaseStorage.save(actor);
        databaseStorage.saveActorMovies(actor);
        return "redirect:/getActorList";
    }

    @RequestMapping("/saveMovie")
    public String saveMovie(HttpServletRequest request) throws Exception {
        Movie movie = new Movie();
        movie.setName(request.getParameter("name"));
        movie.setAuthor(request.getParameter("author"));

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = format.parse(request.getParameter("createDate"));
        movie.setCreateDate(new java.sql.Date(parsed.getTime()));

        for (String actorId : request.getParameterValues("selectedActors")) {
            movie.addActor(databaseStorage.get(Actor.class, Integer.parseInt(actorId)));
        }

        databaseStorage.save(movie);

        databaseStorage.saveMovieActors(movie);

        return "redirect:/getMovieList";
    }

    @RequestMapping("/getActorList")
    public ModelAndView getActorList() throws Exception {
        List<Actor> actors = databaseStorage.actorList();
        return new ModelAndView("actorList", "actors", actors);
    }

    @RequestMapping("/getMovieList")
    public ModelAndView getMovieList() throws Exception {
        List<Movie> movies = databaseStorage.movieList();
        return new ModelAndView("movieList", "movies", movies);
    }

    @RequestMapping("/editActor")
    public ModelAndView editActor(@RequestParam String id) throws Exception {
        Actor actor = databaseStorage.getActorWithMovies(Integer.parseInt(id));
        List<Movie> notAddedMovies = databaseStorage.list(Movie.class);
        List<Movie> addedMovies = new ArrayList<>();
        for (Movie movie : notAddedMovies) {
            for (Movie actorMovie : actor.getMovies()) {
                if (movie.getId() == actorMovie.getId()) {
                    addedMovies.add(movie);
                    break;
                }
            }
        }
        for (Movie addedMovie : addedMovies) {
            notAddedMovies.remove(addedMovie);
        }
        ModelAndView modelAndView = new ModelAndView("editActor");
        modelAndView.addObject("editActor", actor);
        modelAndView.addObject("notAddedMovies", notAddedMovies);
        return modelAndView;
    }

    @RequestMapping("/editMovie")
    public ModelAndView editMovie(@RequestParam String id) throws Exception {
        Movie movie = databaseStorage.getMovieWithActors(Integer.parseInt(id));
        return new ModelAndView("editMovie", "editMovie", movie);
    }

    @RequestMapping("/updateActor")
    public String updateActor(HttpServletRequest request, @ModelAttribute Actor editActor) throws Exception {
        System.out.println("actor name - " + editActor.getFirstName());
        System.out.println("actor last name - " + editActor.getLastName());
        System.out.println(editActor.getMovies().size());
        for (Movie movie : editActor.getMovies()) {
            System.out.println(movie.getName());
        }
        editActor.setFirstName(request.getParameter("firstName"));
        editActor.setLastName(request.getParameter("lastName"));
        databaseStorage.save(editActor);
        return "redirect:/getActorList";
    }

    @RequestMapping("/updateMovie")
    public String updateMovie(HttpServletRequest request) throws Exception {
        Movie movie = new Movie();
        movie.setName(request.getParameter("name"));
        movie.setAuthor(request.getParameter("author"));

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date parsed = format.parse(request.getParameter("createDate"));
        movie.setCreateDate(new java.sql.Date(parsed.getTime()));

        databaseStorage.save(movie);
        return "redirect:/getMovieList";
    }

    @RequestMapping("/deleteActor")
    public String deleteUser(HttpServletRequest request) throws Exception {
        databaseStorage.delete("actor", request.getParameter("id"));
        return "redirect:/getActorList";
    }

    @RequestMapping("/deleteMovie")
    public String deleteMovie(HttpServletRequest request) throws Exception {
        databaseStorage.delete("movie", request.getParameter("id"));
        return "redirect:/getMovieList";
    }
}