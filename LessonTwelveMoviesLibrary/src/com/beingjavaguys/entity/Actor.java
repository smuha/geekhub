package com.beingjavaguys.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Actor extends Entity {

    private String firstName;
    private String lastName;
    private Date birthday;
    @Ignore
    private List<Movie> movies;

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void addMovie(Movie movie){
        movies.add(movie);
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public Actor() {
        movies = new ArrayList<>();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
