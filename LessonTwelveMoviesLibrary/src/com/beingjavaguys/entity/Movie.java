package com.beingjavaguys.entity;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 28.02.14.
 */
public class Movie extends Entity{
    private String name;
    private Date createDate;
    private String author;
    @Ignore
    private List<Actor> actors;

    public Movie() {
        actors = new ArrayList<>();
    }

    public void addActor(Actor actor){
        actors.add(actor);
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
