<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Add actor</title>
    <style>
        body {
            font-size: 20px;
            color: teal;
            font-family: Calibri;
        }

        td {
            font-size: 15px;
            color: black;
            width: 100px;
            height: 22px;
            text-align: left;
        }

        .heading {
            font-size: 18px;
            color: white;
            font: bold;
            background-color: orange;
            border: thick;
        }
    </style>
</head>
<body>
<center>
    <br /> <br /> <br /> <b>Add actor </b> <br />
    <br />
    <div>
        <form method="post" action="/twelve/saveActor">
            <table>
                <tr>
                    <td>Actor first name:</td>
                    <td><input type="text" name="firstName" /></td>
                </tr>
                <tr>
                    <td>Actor last name:</td>
                    <td><input type="text" name="lastName" /></td>
                </tr>
                <tr>
                    <td>Birthday:</td>
                    <td><input type="date" name="birthday" /></td>
                </tr>
                <tr>
                    <td>Actor movies</td>
                    <td>
                        <select name='selectedMovies' multiple>
                            <c:forEach items="${movies}" var="movie">
                                <option value="${movie.id}">${movie.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" value="Save" /></td>
                </tr>
                <tr>
                    <td colspan="5"><a href="/twelve/getActorList">Click Here to See Actor List</a></td>
                    <td></td>
                </tr>
            </table>
        </form>
    </div>
</center>
</body>
</html>