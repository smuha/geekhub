<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <title>Edit movie</title>
    <style>
        body {
            font-size: 20px;
            color: teal;
            font-family: Calibri;
        }

        td {
            font-size: 15px;
            color: black;
            width: 100px;
            height: 22px;
            text-align: center;
        }

        .heading {
            font-size: 18px;
            color: white;
            font: bold;
            background-color: orange;
            border: thick;
        }
    </style>
</head>
<body>
<center>
    <br/> <br/> <br/> <b>Edit movie </b><br/> <br/>

    <table>
        <form action="/twelve/updateMovie" method="post">
            <tr>
                <td>Movie name</td>
                <td><input type="text" name="name" value="${editMovie.name}"></td>
            </tr>
            <tr>
                <td>Creation date</td>
                <td><input type="date" name="createDate" value="${editMovie.createDate}"></td>
            </tr>
            <tr>
                <td>Author</td>
                <td><input type="text" name="author" value="${editMovie.author}"></td>
            </tr>
            <tr>
                <td>Movie actors</td>
                <td>
                    <c:forEach var="actor" items="${editMovie.actors}">
                        ${actor.firstName} ${actor.lastName}<br>
                    </c:forEach>
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Save" /></td>
            </tr>
            <tr>
                <td colspan="2"><a href="/twelve/getMovieList">Click Here to See Movie List</a></td>
            </tr>
        </form>
    </table>

</center>
</body>
</html>