<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Actor list</title>
<style>
body {
	font-size: 20px;
	color: teal;
	font-family: Calibri;
}

td {
	font-size: 15px;
	color: black;
	width: 100px;
	height: 22px;
	text-align: center;
}
.heading {
	font-size: 18px;
	color: white;
	font: bold;
	background-color: orange;
	border: thick;
}
</style>
</head>
<body>
	<center>
		<h2><b>Actor list</b></h2>

		<table border="1">
			<tr>
				<td class="heading">User Id</td>
				<td class="heading">First Name</td>
				<td class="heading">Last Name</td>
				<td class="heading">Birthday</td>
				<td class="heading"># for movies</td>
				<td class="heading">Edit</td>
				<td class="heading">Delete</td>
			</tr>
			<c:forEach var="actor" items="${actors}">
				<tr>
					<td>${actor.id}</td>
					<td>${actor.firstName}</td>
					<td>${actor.lastName}</td>
					<td>${actor.birthday}</td>
                    <td>
                        <c:forEach var="movie" items="${actor.movies}">
                            ${movie.name} <br>
                        </c:forEach>
                    </td>
					<td><a href="/twelve/editActor?id=${actor.id}">Edit</a></td>
					<td><a href="/twelve/deleteActor?id=${actor.id}">Delete</a></td>
				</tr>
			</c:forEach>
			<tr><td colspan="7"><a href="/twelve/addActor">Add New Actor</a></td></tr>
		</table>
        <br><a href="/twelve/getMovieList">Movie List</a>
	</center>
</body>
</html>