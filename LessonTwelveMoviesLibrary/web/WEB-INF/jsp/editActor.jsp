<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <title>Edit actor</title>
    <style>
        body {
            font-size: 20px;
            color: teal;
            font-family: Calibri;
        }

        td {
            font-size: 15px;
            color: black;
            width: 100px;
            height: 22px;
            text-align: center;
        }

        .heading {
            font-size: 18px;
            color: white;
            font: bold;
            background-color: orange;
            border: thick;
        }
    </style>
</head>
<body>
<center>
    <br/> <br/> <br/> <b>Edit actor </b><br/> <br/>

    <table>
        <form:form method="post" action="/twelve/updateActor" modelAttribute="editActor">
            <tr>
                <td>Actor first name</td>
                <td><form:input path="firstName"/></td>
            </tr>
            <tr>
                <td>Actor last name</td>
                <td><form:input path="lastName"/></td>
            </tr>
            <%--<tr>--%>
                <%--<td>Birthday</td>--%>
                <%--<td><form:input path="birthday"/></td>--%>
            <%--</tr>--%>
            <tr>
                <td>Actor movies</td>
                <td>
                    <select name='selectedMovies' multiple>
                        <c:forEach items="${notAddedMovies}" var="movie">
                            <option value="${movie.id}">${movie.name}</option>
                        </c:forEach>
                    </select>
                    _________________________
                    <c:forEach var="movie" items="${editActor.movies}">
                        ${movie.name}<br>
                    </c:forEach>
                </td>
            </tr>
            <tr>
                <td colspan="10"><input type="submit" value="Save"/></td>
            </tr>
            <tr>
                <td colspan="10"><a href="/twelve/getActorList">Click Here to See Actor List</a></td>
            </tr>
        </form:form>
    </table>

</center>
</body>
</html>