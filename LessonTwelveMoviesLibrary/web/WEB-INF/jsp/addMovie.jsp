<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>Add movie</title>
<style>
body {
	font-size: 20px;
	color: teal;
	font-family: Calibri;
}

td {
	font-size: 15px;
	color: black;
	width: 100px;
	height: 22px;
	text-align: left;
}

.heading {
	font-size: 18px;
	color: white;
	font: bold;
	background-color: orange;
	border: thick;
}
</style>
</head>
<body>
	<center>
		<br /> <br /> <br /> <b>Add movie</b> <br />
		<br />
		<div>
			<form method="post" action="/twelve/saveMovie">
				<table>
					<tr>
						<td>Movie name:</td>
						<td><input type="text" name="name" /></td>
					</tr>
                    <tr>
                        <td>Creation date:</td>
                        <td><input type="date" name="createDate" /></td>
                    </tr>
                    <tr>
                        <td>Movie author:</td>
                        <td><input type="text" name="author" /></td>
                    </tr>
                    <tr>
                        <td>Actors in this movie</td>
                        <td>
                            <select name='selectedActors' multiple>
                                <c:forEach items="${actors}" var="actor">
                                    <option value="${actor.id}">${actor.firstName} ${actor.lastName}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
						<td>&nbsp;</td>
						<td><input type="submit" value="Save" /></td>
					</tr>
					<tr>
						<td colspan="2"><a href="/twelve/getMovieList">Click Here to See Movie List</a></td>
					</tr>
				</table>
			</form>
		</div>
	</center>
</body>
</html>