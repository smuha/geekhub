<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Movie list</title>
    <style>
        body {
            font-size: 20px;
            color: teal;
            font-family: Calibri;
        }

        td {
            font-size: 15px;
            color: black;
            width: 100px;
            height: 22px;
            text-align: center;
        }

        .heading {
            font-size: 18px;
            color: white;
            font: bold;
            background-color: orange;
            border: thick;
        }
    </style>
</head>
<body>
<center>
    <h2><b>Movie List</b></h2>

    <table border="1">
        <tr>
            <td class="heading">Movie Id</td>
            <td class="heading">Movie name</td>
            <td class="heading">Creation date</td>
            <td class="heading">Movie author</td>
            <td class="heading"># of actors</td>
            <td class="heading">Edit</td>
            <td class="heading">Delete</td>
        </tr>
        <c:forEach var="movie" items="${movies}">
            <tr>
                <td>${movie.id}</td>
                <td>${movie.name}</td>
                <td>${movie.createDate}</td>
                <td>${movie.author}</td>
                <td>
                    <c:forEach var="actor" items="${movie.actors}">
                        <nobr>${actor.firstName} ${actor.lastName}</nobr> <br>
                    </c:forEach>
                </td>
                <td><a href="/twelve/editMovie?id=${movie.id}">Edit</a></td>
                <td><a href="/twelve/deleteMovie?id=${movie.id}">Delete</a></td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="7"><a href="/twelve/addMovie">Add New Movie</a></td>
        </tr>
    </table>
    <br><a href="/twelve/getActorList">Actor List</a>

</center>
</body>
</html>