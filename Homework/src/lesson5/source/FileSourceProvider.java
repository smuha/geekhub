package lesson5.source;

import java.io.*;
import java.net.URL;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File file = new File(pathToSource);
        return file.canRead();
    }

    @Override
    public String load(String pathToSource) throws IOException {
        StringBuffer finalText = new StringBuffer();
        File file = new File(pathToSource);
        try (BufferedReader in = new BufferedReader(new FileReader(file))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                finalText.append(inputLine);
            }
        } catch (IOException e) {
            System.out.println("Error in loading! (FileSourceProvider)");
            throw new IOException();
        }
        return finalText.toString();
    }
}

