package lesson2;

public interface Driveable {
    void accelerate(int accelerateSpeed);

    void brake(int reduceSpeed);

    void turn(int direction);
}
