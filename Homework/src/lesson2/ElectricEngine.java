package lesson2;

/**
 * Created with IntelliJ IDEA.
 * User: Администратор
 * Date: 22.10.13
 * Time: 2:01
 * To change this template use File | Settings | File Templates.
 */
public class ElectricEngine implements ForceProvider {
    private boolean engineInAction;

    public boolean getEngineInAction() {
        return engineInAction;
    }

    public void setEngineInAction(boolean engineInAction) {
        this.engineInAction = engineInAction;
    }

    ElectricEngine(boolean engineInAction){
        this.engineInAction = engineInAction;
    }
}
