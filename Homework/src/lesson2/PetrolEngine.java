package lesson2;

public class PetrolEngine implements ForceProvider {
    private boolean engineInAction;

    public boolean getEngineInAction() {
        return engineInAction;
    }

    public void setEngineInAction(boolean engineInAction) {
        this.engineInAction = engineInAction;
    }

    PetrolEngine(boolean engineInAction){
        this.engineInAction = engineInAction;
    }
}
