package lesson2;

public class Boat extends Vehicle {
    private Propeller propeller;
    private GasTank gasTank;
    private PetrolEngine petrolEngine;

    Boat(int balanceFuel, int maxFuel, int maxSpeed) {
        this.propeller = new Propeller(maxSpeed);
        this.gasTank = new GasTank(balanceFuel, maxFuel);
        this.petrolEngine = new PetrolEngine(false);
    }

    public int getDirection() {
        return propeller.getDirection();
    }

    public int getSpeed() {
        return propeller.getSpeed();
    }

    public int getCurrentFuel() {
        return gasTank.getCurrentFuel();
    }

    public boolean getEngineInAction() {
        return petrolEngine.getEngineInAction();
    }

    @Override
    public void accelerate(int accelerateSpeed) {
        if (gasTank.getCurrentFuel() == 0) {
            propeller.setSpeed(0);
            petrolEngine.setEngineInAction(false);
        } else {
            propeller.addSpeed(accelerateSpeed);
            gasTank.burnFuel(1);
            petrolEngine.setEngineInAction(true);
        }
    }

    @Override
    public void brake(int reduceSpeed) {
        propeller.reduceSpeed(reduceSpeed);
        if (propeller.getSpeed() == 0) {
            petrolEngine.setEngineInAction(false);
            propeller.setDirection(0);
        }
    }

    @Override
    public void turn(int direction) {
        propeller.setDirection(direction);
        propeller.reduceSpeed(1);  //when the boat turns, its speed decreases.
    }
}
