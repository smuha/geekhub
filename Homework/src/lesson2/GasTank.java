package lesson2;

public class GasTank implements EnergyProvider{
    private int currentFuel;
    private int maxFuel;

    GasTank(int currentFuel, int maxFuel) {
        this.maxFuel = maxFuel;
        if(currentFuel>=maxFuel){
            this.currentFuel = maxFuel;
        } else {
            this.currentFuel = currentFuel;
        }

    }

    public int getMaxFuel() {
        return maxFuel;
    }

    public int getCurrentFuel() {
        return currentFuel;
    }

    public boolean addFuel(int amountFuel) {
        if (amountFuel + currentFuel > this.maxFuel) {
            this.currentFuel += amountFuel;
            return true;
        } else {
            return false;
        }

    }

    public void burnFuel(int amountFuel) {
        if ((amountFuel >= 0) && (amountFuel <= currentFuel)) {
            this.currentFuel -= amountFuel;
        } else if(amountFuel > this.currentFuel){
            this.currentFuel = 0;
        }
    }
}
