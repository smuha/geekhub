package lesson2;

public class Propeller implements ForceAcceptor{
    private int speed;
    private int maxSpeed;
    private int direction; //-1 - right; 1 - left; 0 - straight

    Propeller(int maxSpeed) {
        this.maxSpeed=maxSpeed;
        this.speed = 0;
        this.direction = 0;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        if(direction>=1){
            this.direction = 1;
        } else if(direction<=-1){
            this.direction = direction;
        } else {
            this.direction = direction;
        }
    }

    public int getSpeed() {
        return speed;
    }

    public void addSpeed(int accelerate) {
        if((accelerate+this.speed)>=this.maxSpeed){
            this.speed=this.maxSpeed;
        } else if(accelerate>=0){
            this.speed += accelerate;
        }
    }

    public void reduceSpeed(int reduce) {
        if((this.speed - reduce)<=0){
            this.speed=0;
        }  else if(reduce>=0){
            this.speed -=reduce;
        }
    }
}
