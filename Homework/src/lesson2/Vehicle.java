package lesson2;

public abstract class Vehicle implements Driveable {

    public int fuelConsumption() {
        return 0;
    }

    public void fuelConsumption(int consumption) {
    }

    public int getDirection() {
        return 0;
    }

    public void setDirection(int direction) {
    }

    public int getCurrentFuel() {
        return 0;
    }

    public int getSpeed() {
        return 0;
    }

    public void addSpeed(int speed) {
    }

    public void reduceSpeed(int reduceSpeed){
    }

    public boolean getEngineInAction() {
        return false;
    }

    @Override
    public void accelerate(int accelerateSpeed) {

    }

    @Override
    public void brake(int reduceSpeed) {

    }

    @Override
    public void turn(int direction) {

    }
}

