package lesson1;

import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {
        System.out.print("Введіть число: ");
        Scanner input= new Scanner(System.in);
        int value=input.nextInt();
        System.out.print("Факторіал цього числа: "+factorial(value));
    }

    private static int factorial(int fc){
        if((fc==1)||(fc==0))
            return 1;
        else
            return factorial(fc-1)*fc;
    }
}
