package lesson3.task3;

public interface Driveable {
    void accelerate(int accelerateSpeed);

    void brake(int reduceSpeed);

    void turn(int direction);


}
