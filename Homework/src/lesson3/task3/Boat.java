package lesson3.task3;

public class Boat extends Vehicle {
    private Propeller propeller;
    private GasTank gasTank;
    private PetrolEngine petrolEngine;

    Boat(int balanceFuel, int maxFuel, int maxSpeed, float fuelConsumption) {
        this.propeller = new Propeller(maxSpeed);
        this.gasTank = new GasTank(balanceFuel, maxFuel);
        this.petrolEngine = new PetrolEngine(false, fuelConsumption);
    }

    @Override
    public int getPassedWay() {
        return propeller.getPassedWay();
    }

    @Override
    public void setPassedWay(int passedWay) {
        propeller.setPassedWay(passedWay);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void drive(int passedWay) {
        if (petrolEngine.getEngineInAction()) {
            gasTank.burnFuel(petrolEngine.getFuelConsumption());
            propeller.setPassedWay(passedWay);
        } else {
            System.out.println("RAISE YOUR SPEED! Engine stopped!\n");
        }
    }

    @Override
    public float getFuelConsumption() {
        return petrolEngine.getFuelConsumption();
    }

    @Override
    public float getFuel() {
        return gasTank.getFuel();
    }

    @Override
    public void addFuel(float fuel) {
        gasTank.addFuel(fuel);
    }

    @Override
    public int getDirection() {
        return propeller.getDirection();
    }

    @Override
    public int getSpeed() {
        return propeller.getSpeed();
    }

    @Override
    public boolean getEngineInAction() {
        return petrolEngine.getEngineInAction();
    }

    @Override
    public void accelerate(int accelerateSpeed) {
        if (gasTank.getFuel() == 0) {
            propeller.setSpeed(0);
            petrolEngine.setEngineInAction(false);
        } else {
            propeller.addSpeed(accelerateSpeed);
            petrolEngine.setEngineInAction(true);
        }
    }

    @Override
    public void brake(int reduceSpeed) {
        propeller.reduceSpeed(reduceSpeed);
        if (propeller.getSpeed() == 0) {
            petrolEngine.setEngineInAction(false);
            propeller.setDirection(0);
        }
    }

    @Override
    public void turn(int direction) {
        propeller.setDirection(direction);
        propeller.reduceSpeed(1);  //when the boat turns, its speed decreases.
    }
}
