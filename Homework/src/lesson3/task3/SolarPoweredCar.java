package lesson3.task3;


public class SolarPoweredCar extends Vehicle {
    private Wheel wheel;
    private SolarBattery solarBattery;
    private ElectricEngine electricEngine;

    SolarPoweredCar(int batteryCharge, int maxBatteryCharge, int maxSpeed, float fuelConsumption) {
        this.wheel = new Wheel(maxSpeed);
        this.solarBattery = new SolarBattery(batteryCharge, maxBatteryCharge);
        this.electricEngine = new ElectricEngine(false, fuelConsumption);
    }

    @Override
    public int getPassedWay() {
        return wheel.getPassedWay();
    }

    @Override
    public void setPassedWay(int passedWay) {
        wheel.setPassedWay(passedWay);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void drive(int passedWay){
        if(electricEngine.getEngineInAction()){
            solarBattery.burnFuel(electricEngine.getFuelConsumption());
            wheel.setPassedWay(passedWay);
        }
        else{
            System.out.println("RAISE YOUR SPEED! Engine stopped!\n");
        }
    }

    @Override
    public float getFuel() {
        return solarBattery.getFuel();
    }

    @Override
    public float getFuelConsumption() {
        return electricEngine.getFuelConsumption();
    }

    @Override
    public void addFuel(float fuel) {
        solarBattery.addFuel(fuel);
    }

    @Override
    public int getDirection() {
        return wheel.getDirection();
    }

    @Override
    public boolean getEngineInAction() {
        return electricEngine.getEngineInAction();
    }

    @Override
    public int getSpeed() {
        return wheel.getSpeed();
    }

    @Override
    public void turn(int direction) {
        wheel.setDirection(direction);
        wheel.reduceSpeed(1);//when the actionWithCar turns, its speed decreases.
    }

    @Override
    public void brake(int reduceSpeed) {
        wheel.reduceSpeed(reduceSpeed);
        if (wheel.getSpeed() == 0) {
            electricEngine.setEngineInAction(false);
            wheel.setDirection(0);
        }
    }

    @Override
    public void accelerate(int accelerateSpeed) {
        if (solarBattery.getFuel() == 0) {
            wheel.setSpeed(0);
            electricEngine.setEngineInAction(false);
        } else {
            wheel.addSpeed(accelerateSpeed);
            electricEngine.setEngineInAction(true);
        }
    }
}
