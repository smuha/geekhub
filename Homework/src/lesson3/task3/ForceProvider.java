package lesson3.task3;

public interface ForceProvider {
    public float getFuelConsumption();

    public boolean getEngineInAction();

    public void setEngineInAction(boolean engineInAction);
}
