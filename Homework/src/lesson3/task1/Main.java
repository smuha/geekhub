package lesson3.task1;

import java.util.Random;
import java.util.Scanner;

public class Main {

    private static Comparable[] sort(Comparable[] comp) {
        Comparable[] temporaryComp = new Comparable[comp.length];
        temporaryComp = comp.clone();

        for (int i=0; i<comp.length; i++){
            for (int j=0; j<comp.length-1; j++){
                if(temporaryComp[j].compareTo(temporaryComp[j + 1]) == 1){
                    Comparable t =temporaryComp[j];
                    temporaryComp[j]=temporaryComp[j+1];
                    temporaryComp[j+1]=t;
                }
            }
        }
        return temporaryComp;
    }

    public static void main(String[] args) {
        Random random = new Random();
        Scanner input;
        while (true){
            System.out.println("\nChoose class you want:");
            System.out.println(" 1 - Owner");
            System.out.println(" 2 - Car");
            System.out.println(" 3 - Exit");
            System.out.print("Your choose: ");
            input = new Scanner(System.in);
            switch (input.nextInt()){
                case 1:
                    workWithOwners(random);
                    break;
                case 2:
                    workWithCars(random);
                    break;
                case 3:
                    System.exit(0);
            }
        }
    }

    private static void workWithOwners(Random random) {
        Owner[] owners = new Owner[10];
        fillingOwners(owners, random);
        System.out.println("Initial array:");
        showOwners(owners);
        owners = (Owner[]) sort(owners);
        System.out.println("Array sorted by owners age:");
        showOwners(owners);
    }

    private static void workWithCars(Random random) {
        Car[] cars = new Car[10];
        fillingCars(random, cars);
        System.out.println("Initial array:");
        showCars(cars);
        cars = (Car[]) sort(cars);
        System.out.println("Array sorted by cars max speed:");
        showCars(cars);
    }

    private static void fillingCars(Random random, Car[] cars) {
        for (int i = 0; i < cars.length; i++) {
            cars[i] = new Car(random.nextInt(360),random.nextInt(5000));
        }
        cars[0].setName("Toyota");
        cars[1].setName("BMB");
        cars[2].setName("Subaru");
        cars[3].setName("Nisan");
        cars[4].setName("Audi");
        cars[5].setName("Honda");
        cars[6].setName("Chery");
        cars[7].setName("Dacha");
        cars[8].setName("Sidan");
        cars[9].setName("Lada");
    }

    private static void showCars(Car[] cars) {
        System.out.println(" Name\tMax speed weight");
        for(int i=0; i < cars.length; i++){
            System.out.print(" "+cars[i].getName()+"\t");
            System.out.print(cars[i].getSpeed()+"\t \t  ");
            System.out.println(cars[i].getWeight());
        }
    }

    private static void showOwners(Owner[] owners) {
        System.out.println(" Name\tAge");
        for(int i=0; i < owners.length; i++){
            System.out.print(" "+owners[i].getName()+"\t");
            System.out.println(owners[i].getAge());
        }
    }

    private static void fillingOwners(Owner[] owners, Random random) {
        for (int i = 0; i < owners.length; i++) {
            owners[i] = new Owner();
            owners[i].setAge(random.nextInt(100));
        }
        owners[0].setName("Slava");
        owners[1].setName("Kolya");
        owners[2].setName("Vanya");
        owners[3].setName("Anton");
        owners[4].setName("Sasha");
        owners[5].setName("Vova");
        owners[6].setName("Valera");
        owners[7].setName("Bogdan");
        owners[8].setName("Borya");
        owners[9].setName("Olexiy");
    }
}
