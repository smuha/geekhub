package lesson6.json.adapters;

import lesson6.json.JsonSerializer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

/**
 * Converts all objects that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {
    @Override
    public Object toJson(Collection collection) throws JSONException{
        JSONArray result = new JSONArray();
        for (Object o : collection ){
            result.put(JsonSerializer.serialize(o));
        }
        return result;
    }
}
