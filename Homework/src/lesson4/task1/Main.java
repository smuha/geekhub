package lesson4.task1;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Main {
    private static MySetOperations operations = new MySetOperations();

    public static void main(String[] args) {
        comparingCollections();
        mergingCollections();
        subtractBFromA();
        intersectionOfSets();
        symmetricSubtract();
    }

    private static void fillingCollection(Set a, Set b) {
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            a.add(random.nextInt(10));
            b.add(random.nextInt(10));
        }
    }

    private static void symmetricSubtract() {
        System.out.println("Method symmetricSubtract():");
        Set a = new HashSet();
        Set b = new HashSet();
        fillingCollection(a, b);
        System.out.println(" Collection A - " + a);
        System.out.println(" Collection B - " + b);
        System.out.println(" A Δ B - " + operations.symmetricSubtract(a, b));
    }

    private static void intersectionOfSets() {
        System.out.println("Method intersect():");
        Set a = new HashSet();
        Set b = new HashSet();
        fillingCollection(a, b);
        System.out.println(" Collection A - " + a);
        System.out.println(" Collection B - " + b);
        System.out.println(" A ∩ B - " + operations.intersect(a, b));
    }

    private static void subtractBFromA() {
        System.out.println("Method subtract():");
        Set a = new HashSet();
        Set b = new HashSet();
        fillingCollection(a, b);
        System.out.println(" Collection A - " + a);
        System.out.println(" Collection B - " + b);
        System.out.println(" A \\ B - " + operations.subtract(a, b));
    }

    private static void mergingCollections() {
        System.out.println("Method union():");
        Set a = new HashSet();
        Set b = new HashSet();
        fillingCollection(a, b);
        System.out.println(" Collection A - " + a);
        System.out.println(" Collection B - " + b);
        System.out.println(" A ∪ B - " + operations.union(a, b));
    }

    private static void comparingCollections() {
        Random random = new Random();
        System.out.println("Method equals():");
        Set a = new HashSet();
        Set b = new HashSet();
        for (int i = 0; i < 2; i++) {
            a.add(random.nextInt(2));
            b.add(random.nextInt(2));
        }
        System.out.println(" Collection A - " + a);
        System.out.println(" Collection B - " + b);
        System.out.println(" Collections are " + (operations.equals(a, b) ? "equal!" : "not equal!"));
    }
}
