package lesson4.task2;

import sun.java2d.pipe.SpanShapeRenderer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    private static MyTaskManager taskManager = new MyTaskManager();

    public static void main(String[] args) {

        Task task1 = new Task("work");
        task1.setDescription("Meeting with client");
        taskManager.addTask(new GregorianCalendar(2013, 11, 9), task1);

        Task task2 = new Task("university");
        task2.setDescription("Meeting with teacher");
        taskManager.addTask(new GregorianCalendar(2013, 11, 6), task2);

        Task task3 = new Task("sport");
        task3.setDescription("Tennis competition");
        taskManager.addTask(new GregorianCalendar(2013, 11, 7), task3);

        Task task4 = new Task("sport");
        task4.setDescription("Go to gym");
        taskManager.addTask(new GregorianCalendar(2013, 11, 10), task4);

        Task task5 = new Task("rest");
        task5.setDescription("Go to the cinema");
        taskManager.addTask(new GregorianCalendar(2013, 11, 8), task5);

        System.out.println("Method getCategories():\n" + taskManager.getCategories());
        System.out.println("Method getTasksByCategories():\n" + taskManager.getTasksByCategories());
        System.out.println("Method getTaskByCategory('sport'):\n" + taskManager.getTasksByCategory("sport"));
        System.out.println("Method getTaskForToday(): // 2013, 11, 8\n" + taskManager.getTasksForToday(new GregorianCalendar(2013, 11, 8)));
        System.out.println("Method removeTask(2013,11,9): // work - Meeting with client");
        taskManager.removeTask(new GregorianCalendar(2013, 11, 9));
        System.out.println("Method getCategories():\n" + taskManager.getCategories());
    }

    private static void showDate(Calendar date) {
        System.out.print(date.get(Calendar.DATE) + ".");
        System.out.print(date.get(Calendar.MONTH) + ".");
        System.out.print(date.get(Calendar.YEAR));
    }
}
